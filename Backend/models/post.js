const mongoose = require('mongoose');

const Post = mongoose.model('Post', {
    title: {
        type: String,
        required: true
    },

    description: {
        type: String,
    },
    imagePath: {
        type: String,
        required: true
    },
    lookingFor:{
        type: Array,
    },
    postDate: {
        type: String,
        required: true
    },

    creator: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true }
});


module.exports = Post